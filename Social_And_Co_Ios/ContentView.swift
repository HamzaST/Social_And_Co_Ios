//
//  ContentView.swift
//  Social_And_Co_Ios
//
//  Created by HAMZA on 10/8/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
