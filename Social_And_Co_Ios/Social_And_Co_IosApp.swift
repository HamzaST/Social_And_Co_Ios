//
//  Social_And_Co_IosApp.swift
//  Social_And_Co_Ios
//
//  Created by HAMZA on 10/8/2022.
//

import SwiftUI

@main
struct Social_And_Co_IosApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
